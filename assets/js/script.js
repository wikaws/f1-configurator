// Handles loading the events for <model-viewer>'s slotted progress bar
const onProgress = (event) => {
  const progressBar = event.target.querySelector('.progress-bar');
  const updatingBar = event.target.querySelector('.update-bar');
  updatingBar.style.width = `${event.detail.totalProgress * 100}%`;
  if (event.detail.totalProgress === 1) {
    progressBar.classList.add('hide');
    event.target.removeEventListener('progress', onProgress);
  } else {
    progressBar.classList.remove('hide');
  }
};
document.querySelector('model-viewer').addEventListener('progress', onProgress);

const modelViewer = document.querySelector('model-viewer');
const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
const urlParams = new URLSearchParams(window.location.search);
const colorHex = urlParams.get('colorHex') == null ? '000000' : urlParams.get('colorHex');

if(isMobile){
  modelViewer.addEventListener('load', () => {
    // document.getElementById('loadingBar').style.display = 'none';
    console.log(colorHex);

    const material = modelViewer.model.materials[4];
    if (material != null) {
      material.pbrMetallicRoughness.
        setBaseColorFactor('#'+colorHex);
        // document.getElementById("qrcode-image").src = "./assets/images/qrcode-"+colorHex+".png";
    }

    // if(isMobile) {
    //   document.querySelector('.c-wrapper').style.display = 'none';
    // }

    if (modelViewer.canActivateAR) {
      modelViewer.activateAR();
    } else {
      console.error('AR mode is not supported.');
    }

    const whichMaterial = (event) => {
      console.log(modelViewer.materialFromPoint(event.clientX, event.clientY));
    };

    modelViewer.addEventListener("click", whichMaterial);
  });
}else{
  document.getElementById("popUpDesc").style.display = 'flex';
  document.getElementById("qrcode-image").src = 'assets/images/qrcodes/qr-code-'+colorHex+'.png';
}